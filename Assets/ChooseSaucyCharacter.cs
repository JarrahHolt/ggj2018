﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseSaucyCharacter : MonoBehaviour {

	public static int charIndex = 0;

	public List<GameObject> charList = new List<GameObject>();

	// Use this for initialization
	void Start () {
		DeactivateAll ();

		if (charList.Count >= charIndex - 1)
			charList [charIndex].SetActive (true);
	}
	
	public void DeactivateAll()
	{
		for (int i = 0; i < charList.Count; i++) {
			charList [i].SetActive (false);
		}
	}
}
