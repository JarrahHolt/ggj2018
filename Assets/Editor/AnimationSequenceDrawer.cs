﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(AnimationSequence))]
public class AnimationSequenceDrawer : PropertyDrawer {

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {

        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        var animationRect = new Rect(position.x, position.y, ((position.width / 4) * 3), position.height);
        var delayRect = new Rect(position.x + ((position.width / 4) * 3), position.y, position.width / 4, position.height);

        EditorGUI.PropertyField(animationRect, property.FindPropertyRelative("_animation"), GUIContent.none);
        EditorGUI.PropertyField(delayRect, property.FindPropertyRelative("_delay"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

}
