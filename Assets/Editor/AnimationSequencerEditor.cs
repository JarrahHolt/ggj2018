﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(AnimationSequencer),true)]
public class AnimationSequencerEditor : Editor {

    private ReorderableList list;

    private void OnEnable()
    {
        list = new ReorderableList(serializedObject, 
                serializedObject.FindProperty("_animationList"), 
                true, true, true, true);

        list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = list.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            var animationRect = new Rect(rect.x, rect.y, ((rect.width / 5) * 2), EditorGUIUtility.singleLineHeight);
            var animationClipRect = new Rect(rect.x + ((rect.width / 5) * 2), rect.y, ((rect.width / 5)*2), EditorGUIUtility.singleLineHeight);
            var delayRect = new Rect(rect.x + ((rect.width / 5) * 4), rect.y, rect.width / 5, EditorGUIUtility.singleLineHeight);

            EditorGUI.PropertyField(animationRect, element.FindPropertyRelative("_animation"), GUIContent.none);
            EditorGUI.PropertyField(animationClipRect, element.FindPropertyRelative("_animationClip"), GUIContent.none);
            EditorGUI.PropertyField(delayRect, element.FindPropertyRelative("_delay"), GUIContent.none);
        };

        list.drawHeaderCallback = (Rect rect) => { EditorGUI.LabelField(rect, "Animation Sequence"); };
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        list.DoLayoutList();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_onSequenceCompleteAction"));
        serializedObject.ApplyModifiedProperties();
    }

}
