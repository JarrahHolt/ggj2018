﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System;

public class MusicEditor : EditorWindow {

    public TrackSO trackSO;
    private bool musicPlaying;
    private float startTime;

    [MenuItem("Window/My Window")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        MusicEditor window = (MusicEditor)EditorWindow.GetWindow(typeof(MusicEditor));
        window.Show();
    }

    void OnGUI()
    {
        trackSO = (TrackSO)EditorGUILayout.ObjectField(trackSO, typeof(TrackSO), false);
        if (trackSO == null)  { return; }
        if (musicPlaying)
        {
            if (Event.current != null && Event.current.isKey)
            {
                if (Event.current.keyCode == KeyCode.A)
                {
                    NoteData temp = new NoteData();
                    temp.position = 0;
                    temp.hitTime = Time.time - startTime;
                    trackSO.trackData.noteList.Add(temp);
                    EditorUtility.SetDirty(trackSO);
                }
                if (Event.current.keyCode == KeyCode.S)
                {
                    NoteData temp = new NoteData();
                    temp.position = 1;
                    temp.hitTime = Time.time - startTime;
                    trackSO.trackData.noteList.Add(temp);
                    EditorUtility.SetDirty(trackSO);
                }
                if (Event.current.keyCode == KeyCode.K)
                {
                    NoteData temp = new NoteData();
                    temp.position = 2;
                    temp.hitTime = Time.time - startTime;
                    trackSO.trackData.noteList.Add(temp);
                    EditorUtility.SetDirty(trackSO);
                }
                if (Event.current.keyCode == KeyCode.L)
                {
                    NoteData temp = new NoteData();
                    temp.position = 3;
                    temp.hitTime = Time.time - startTime;
                    trackSO.trackData.noteList.Add(temp);
                    EditorUtility.SetDirty(trackSO);
                }
                EditorGUILayout.LabelField(string.Format("Note List Length", trackSO.trackData.noteList.Count));
                EditorGUILayout.LabelField(string.Format("Song Time", Time.time - startTime));
            }
            if (GUILayout.Button("End Song"))
            {
                StopAllClips();
            }
        }
        else if (GUILayout.Button("Start Song"))
        {
            musicPlaying = true;
            PlayClip(trackSO.trackData.song);
            trackSO.trackData.noteList.Clear();
            Undo.RecordObject(trackSO, "Cleared List");
            startTime = Time.time;
        }
    }

    public static void PlayClip(AudioClip clip)
    {
        Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
        Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
        MethodInfo method = audioUtilClass.GetMethod(
            "PlayClip",
            BindingFlags.Static | BindingFlags.Public,
            null,
            new System.Type[] {
         typeof(AudioClip)
        },
        null
        );
        method.Invoke(
            null,
            new object[] {
         clip
        }
        );
    }
    public static void StopAllClips()
    {
        Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
        Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
        MethodInfo method = audioUtilClass.GetMethod(
            "StopAllClips",
            BindingFlags.Static | BindingFlags.Public,
            null,
            new System.Type[] { },
            null
        );
        method.Invoke(
            null,
            new object[] { }
        );
    }

}
