﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerDirection {   Forwards = 1,
                                Backwards = -1,
                                None = 0 };

public class Character : MonoBehaviour {

    public List<ColourSteal> colourSteal;
    public CharacterMotor characterMotor;
    public CharacterInput characterInput;
    public LayerMask collisionMask;
    public Animator animator;
    private Interactable interactable;
    private new Collider collider;


    private void OnTriggerEnter(Collider other)
    {
        if (collisionMask == (collisionMask | (1 << other.gameObject.layer)))
        {
            interactable = other.GetComponent<Interactable>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (collisionMask == (collisionMask | (1 << other.gameObject.layer)))
        {
            interactable = null;
        }
    }

    void OnEnable () {
        characterMotor.InitializePath();
        characterMotor.SetPlayerPosition(-1);
        collider = GetComponent<Collider>();
        SetColourSteal();
	}

    public void SetColourSteal()
    {
        foreach (var item in Enum.GetValues(typeof(CharacterName)))
        {
            ColourSteal temp = colourSteal.Find(x => x.characterName == (CharacterName)item);
            if (temp != null)
            {
                temp.characterColour.SetActive(GameManager.instance.saveData.HasCharacterBeenAbsorbed(temp.characterName));
            }
        }

    }
	
    public void MoveCharacter(PlayerDirection direction)
    {
        characterMotor.SetPlayerPosition((float)direction);
        if (animator != null)
        {
            animator.SetInteger("Direction", (int)direction);
        }
    }

    public void InteractWithObject()
    {
        if (interactable != null)
        {
            interactable.InteractPress();
        }
    }

}

[System.Serializable]
public class ColourSteal
{
    public CharacterName characterName;
    public GameObject characterColour;
}
