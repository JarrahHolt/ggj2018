﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInput : MonoBehaviour {

    private Character _character;
    private Character character
    {
        get
        {
            if (_character == null)
            {
                _character = GetComponent<Character>();
            }
            return _character;
        }
    }

    public void Update()
    {
        HandleMovement();
		HandleInteraction ();
    }

    private void HandleMovement()
    {
        if (Input.GetKey(KeyCode.A))
        {
            character.MoveCharacter(PlayerDirection.Backwards);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            character.MoveCharacter(PlayerDirection.Forwards);
        } else
        {
            character.MoveCharacter(PlayerDirection.None);
        }
    }

    private void HandleInteraction()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            character.InteractWithObject();
        }
    }

}
