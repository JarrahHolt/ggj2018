﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CharacterMotor : MonoBehaviour {

    [SerializeField] private List<Transform> _pathNodes;
    [SerializeField] private float moveSpeed = 5;

    private float _totalDistance;
    private float _normalizedPosition;
    private Vector3[] _pathPositions;

    public void InitializePath()
    {
        _pathPositions = _pathNodes.Select(x => x.position).ToArray();
        _totalDistance = Vector3Extensions.MultiDistance(_pathPositions);
    }

    public void SetPlayerPosition(float direction)
    {
        if (_pathPositions != null && _pathPositions.Length > 0)
        {
            _normalizedPosition += ((moveSpeed / _totalDistance) * direction) * Time.deltaTime;
            _normalizedPosition = Mathf.Clamp01(_normalizedPosition);
            transform.position = GetCharacterPosition(_normalizedPosition);
        }
    }

    private Vector3 GetCharacterPosition(float _normalizedPosition)
    {
        if (_pathPositions != null && _pathPositions.Length > 0)
        {
             return Vector3Extensions.MultiLerp(_pathPositions, _normalizedPosition);
        }
        return Vector3.one;
    }

#if UNITY_EDITOR

    private void OnDrawGizmos()
    {
        if (_pathNodes != null && _pathNodes.Count > 0)
        {
            for (int i = 0; i < _pathNodes.Count; i++)
            {
                UnityEditor.Handles.color = Color.red;
                UnityEditor.Handles.DrawWireDisc(_pathNodes[i].position, Vector3.forward, 0.4f);
                if ((i + 1) < _pathNodes.Count)
                {
                    UnityEditor.Handles.color = Color.white;
                    UnityEditor.Handles.DrawLine(_pathNodes[i].position, _pathNodes[i + 1].position);
                }
            }
            UnityEditor.Handles.DrawWireCube(GetCharacterPosition(_normalizedPosition), Vector3.one);
        }
    }

#endif
}
