﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatePositions : MonoBehaviour
{
	public List<Transform> trianglePositions;
	public List<Transform> starPositions;
	public List<Transform> vvPositions;
	public List<Transform> beanPositions;
	public List<Transform> pentagonPositions;
    public Transform dedTransform;

    void Start()
    {
        SetAllCharactersTransform();
    }

    public void SetAllCharactersTransform()
    {
        foreach (var item in Enum.GetValues(typeof(CharacterName)))
        {
            if (Interactable.characterDictionary.ContainsKey((CharacterName)item))
            {
                Interactable interactable = Interactable.characterDictionary[(CharacterName)item];
                int index = GameManager.instance.saveData.GetCharacterPositionIndex(interactable.characterName);
                Transform endTransform = null;
                switch (interactable.characterName)
                {
                    case CharacterName.Triangle:
                        if (index >= trianglePositions.Count) { endTransform = dedTransform; break; }
                        endTransform = trianglePositions[index];
                        break;
                    case CharacterName.Bean:
                        if (index >= beanPositions.Count) { endTransform = dedTransform; break; }
                        endTransform = beanPositions[index];
                        break;
                    case CharacterName.Star:
                        if (index >= starPositions.Count) { endTransform = dedTransform; break; }
                        endTransform = starPositions[index];
                        break;
                    case CharacterName.Pentagon:
                        if (index >= pentagonPositions.Count) { endTransform = dedTransform; break; }
                        endTransform = pentagonPositions[index];
                        break;
                    case CharacterName.VV:
                        if (index >= vvPositions.Count) { endTransform = dedTransform; break; }
                        endTransform = vvPositions[index];
                        break;
                    default:
                        break;
                }
                interactable.transform.SetParent(endTransform);
                interactable.transform.localPosition = Vector3.zero;
                interactable.transform.localRotation = Quaternion.identity;
                interactable.transform.localScale = Vector3.one;
            }
        }
    }
}
