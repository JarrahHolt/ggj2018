﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

[RequireComponent(typeof(BoxCollider))]
public class Interactable : MonoBehaviour {

    public static Dictionary<CharacterName, Interactable> characterDictionary = new Dictionary<CharacterName, Interactable>();

    public CharacterName characterName;
    public RectTransform _interactButtonTF;
    public LayerMask _collisionMask;
    public UnityEvent _onInteractPress;
	public int currentDate;

    // Marcus Edit
    public AudioClip[] dateSound;
    AudioClip dateSoundClip;

	public TrackSO characterTrack;
	public List<Conversation> conversations = new List<Conversation> ();

    private AudioSource audioSource;
    private Tweener _activeTweener;
    private float _initialScale = 0;
    private float _endScale = 1;
    private float _animationTime = 1;

#if UNITY_EDITOR

    public void OnValidate()
    {
        BoxCollider boxColliderComponent = GetComponent<BoxCollider>();
        boxColliderComponent.isTrigger = true;
    }

#endif

    void Awake()
    {
        audioSource = GameObject.FindObjectOfType<AudioSource>();
        if (characterDictionary != null && characterDictionary.ContainsKey(characterName))
        {
            characterDictionary[characterName] = this;
        } else
        {
            characterDictionary.Add(characterName, this);
        }
    }

    private void OnEnable()
    {
        _interactButtonTF.localScale = Vector3.zero;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_collisionMask == (_collisionMask | (1 << other.gameObject.layer)))
        {
            if (_activeTweener != null) { _activeTweener.Kill(); }
            _activeTweener = DOTween.To(() => Vector3.one * _initialScale, x => _interactButtonTF.localScale = x, Vector3.one * _endScale, _animationTime).SetEase(Ease.OutElastic);

            // array of sounds
            int index = Random.Range(0, dateSound.Length);

            dateSoundClip = dateSound[index];

            audioSource.clip = dateSoundClip;

            audioSource.PlayOneShot(dateSoundClip);
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (_collisionMask == (_collisionMask | (1 << other.gameObject.layer)))
        {
            if (_activeTweener != null) { _activeTweener.Kill(); }
            _activeTweener =DOTween.To(() => Vector3.one * _endScale, x => _interactButtonTF.localScale = x, Vector3.one * _initialScale, _animationTime/4).SetEase(Ease.OutCirc);
        }
    }

    public void InteractPress()
    {
        if (_onInteractPress != null)
        {
            _onInteractPress.Invoke();
        }

		currentDate = GameManager.instance.saveData.GetCharacterPositionIndex (characterName);
		SetConversationProperties (currentDate);
		ChooseSaucyCharacter.charIndex = (int)characterName;

		GameManager.instance.saveData.SetCharacterDateScore (characterName, currentDate + 1, 1f);
    }

	public void SetConversationProperties(int conversationIndex)
	{
		TrackManager.trackToLoad = characterTrack;
		if (conversations.Count >= conversationIndex - 1)
			ConversationManager.conversationToLoad = conversations [conversationIndex];
	}
}
