﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Marcus
public class PlayerMoveTest : MonoBehaviour {



    public float moveSpeed;

    Rigidbody2D rig2D;

	// Use this for initialization
	void Start ()
    {
        rig2D = GetComponent<Rigidbody2D>();
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector2 movement = Vector2.zero;

        if (Input.GetKey(KeyCode.A))
        {
            movement.x = (transform.right * Time.deltaTime * -moveSpeed).x;
        }
        if (Input.GetKey(KeyCode.D))
        {
            movement.x = (transform.right * Time.deltaTime * moveSpeed).x;
        }

        movement = movement + (Vector2)(transform.position);

        rig2D.MovePosition(movement);

    }
}
