﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreenScript : MonoBehaviour {

    public int sceneToLoad;

    void Start()
    {
        LoadMain(); 
    }

    public void LoadMain()
    {
        SceneManager.LoadScene(sceneToLoad, LoadSceneMode.Single);
    }
}
