﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDespawn : MonoBehaviour
{
    public float despawnTime = 1f;

    void Start()
    {
        Invoke("Despawn", despawnTime);
    }

    void Despawn()
    {
        Destroy(gameObject);
    }
}
