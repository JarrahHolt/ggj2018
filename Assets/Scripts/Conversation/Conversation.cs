﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "Conversation", menuName = "Conversation")]
public class Conversation : ScriptableObject
{
	public const char REPLACEMENT_CHAR = '#';

	[Multiline(100)]
	public string dialogue = "0123456789";
	public string endDialogue;

	public void Init()
	{
		endDialogue = dialogue;
	}

	public void ReplaceDialogueAt(int startIndex, int length)
	{
		endDialogue = endDialogue.Remove(startIndex, length).Insert(startIndex, new string(REPLACEMENT_CHAR, length));
	}

	public char[] GetDialogueChars()
	{
		return dialogue.ToCharArray ();
	}
}
