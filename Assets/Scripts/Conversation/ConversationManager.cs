﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConversationManager : MonoBehaviour
{
	public static ConversationManager instance = null;

	public static Conversation conversationToLoad;
	public Conversation currentConversation;

	public Text endDialogueText;

	void Awake()
	{
		if (!instance)
			instance = this;
		else
			Destroy (gameObject);

		if (conversationToLoad != null)
			currentConversation = conversationToLoad;

		currentConversation.Init ();
		Track.onStopTrack += OnStopTrack;
		ConversationManager.SetDialogueText (string.Empty);
	}

	void OnStopTrack()
	{
		ConversationManager.SetDialogueText (currentConversation.endDialogue);
	}

	public static void SetDialogueText(string text)
	{
		instance.endDialogueText.text = text;
	}

	public static void ReplaceDialogueAt(int startIndex, int length)
	{
		instance.currentConversation.ReplaceDialogueAt (startIndex, length);
		SetDialogueText (instance.currentConversation.endDialogue);
	}

	public static int GetCurrentDialogueLength()
	{
		return instance.currentConversation.dialogue.Length;
	}

	void OnDestroy()
	{
		Track.onStopTrack -= OnStopTrack;
	}
}
