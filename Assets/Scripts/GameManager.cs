﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using System.IO;

public class GameManager : MonoBehaviour {

    private static GameManager _instance;
    public static GameManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
                _instance.saveData.LoadData();
            }
            return _instance;
        }
    }

    public SaveDataWrapper saveData = new SaveDataWrapper();

    private void OnDestroy()
    {
        saveData.SaveData();
        if (instance == this)
        {
            _instance = null;
        }
    }

}

public class SaveDataWrapper
{
    public const string SAVE_GAME_LOCATION = "/saveGame.jeff";
    public SaveData saveData = new SaveData();

    public float musicVolume
    {
        get
        {
            return saveData._musicVolume;
        } private set
        {
            saveData._musicVolume = value;
        }
    }

    public float soundVolume
    {
        get
        {
            return saveData._soundVolume;
        }
        private set
        {
            saveData._soundVolume = value;
        }
    }

    public void SetCharacterDateScore(CharacterName name, int date, float score)
    {
        if (saveData._characterDateScores.ContainsKey(name))
        {
            if (saveData._characterDateScores[name] != null && saveData._characterDateScores[name].Count > date)
            {
                saveData._characterDateScores[name][date] = score;
            } else
            {
                saveData._characterDateScores[name].Add(score);
            }
        } else
        {
            saveData._characterDateScores.Add(name, new List<float>());
            saveData._characterDateScores[name].Add(score);
        }
    }

    public int GetCharacterPositionIndex(CharacterName name)
    {
        if (saveData._characterDateScores.ContainsKey(name))
        {
            if (saveData._characterDateScores[name] != null)
            {
                return saveData._characterDateScores[name].Count -1;
            }
        }
        return 0;
    }

    public bool HasCharacterBeenAbsorbed(CharacterName name)
    {
        if (saveData._characterDateScores.ContainsKey(name))
        {
            if (saveData._characterDateScores[name] != null && saveData._characterDateScores[name].Count >= 3)
            {
                float finalScore = 0;
                for (int i = 0; i < saveData._characterDateScores[name].Count; i++)
                {
                    finalScore += saveData._characterDateScores[name][i];
                }
                return (finalScore == 3);
            }
        }
        return false;
    }

    public void SaveData()
    {
        BinaryFormatter bf = new BinaryFormatter();
        using (FileStream file = new FileStream(Application.persistentDataPath + SAVE_GAME_LOCATION, FileMode.OpenOrCreate))
        {
            bf.Serialize(file, saveData);
            file.Close();
            file.Dispose();
        }
    }

    public void LoadData()
    {
        if (File.Exists(Application.persistentDataPath + SAVE_GAME_LOCATION))
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (FileStream file = new FileStream(Application.persistentDataPath + SAVE_GAME_LOCATION, FileMode.OpenOrCreate))
            {
                saveData = (SaveData)bf.Deserialize(file);
                file.Close();
                file.Dispose();
            }
        }
    }

    public void ClearData()
    {
        if (File.Exists(Application.persistentDataPath + SAVE_GAME_LOCATION))
        {
            File.Delete(Application.persistentDataPath + SAVE_GAME_LOCATION);
        }
        saveData = new SaveData();
    }

}

[System.Serializable]
public class SaveData
{
    public Dictionary<CharacterName, List<float>> _characterDateScores = new Dictionary<CharacterName, List<float>>();
    public float _musicVolume;
    public float _soundVolume;
}