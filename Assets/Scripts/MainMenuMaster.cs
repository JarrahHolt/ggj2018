﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenuMaster : MonoBehaviour {

    public GameObject mainMenuPanel;
    public GameObject optionsPanel;
    public GameObject creditsPanel;
    public GameObject askAgainPanel;

    void Awake()
    {
        mainMenuPanel.SetActive(true);

        optionsPanel.SetActive(false);
        askAgainPanel.SetActive(false);
        creditsPanel.SetActive(false);
    }

    public void NewGameButton()
    {
        GameManager.instance.saveData.ClearData();
        SceneManager.LoadScene(2);
    }

    public void LoadGameButton()
    {
        GameManager.instance.saveData.LoadData();
        SceneManager.LoadScene(2);
    }

    public void OptionsButton()
    {
        // Clear Save
        // Volume
        // Credits

        optionsPanel.SetActive(true);

        mainMenuPanel.SetActive(false);
        creditsPanel.SetActive(false);
        askAgainPanel.SetActive(false);

    }

    public void CreditsButton()
    {
        creditsPanel.SetActive(true);

        optionsPanel.SetActive(false);
        mainMenuPanel.SetActive(false);
        askAgainPanel.SetActive(false);

    }

    public void ExitButton()
    {
        askAgainPanel.SetActive(true);

        mainMenuPanel.SetActive(false);
        optionsPanel.SetActive(false);
        creditsPanel.SetActive(false);

    }

    public void BackToMainMenu()
    {
        mainMenuPanel.SetActive(true);

        askAgainPanel.SetActive(false);
        optionsPanel.SetActive(false);
        creditsPanel.SetActive(false);
    }

    public void BackToOptions()
    {
        optionsPanel.SetActive(true);

        creditsPanel.SetActive(false);
        mainMenuPanel.SetActive(false);
        askAgainPanel.SetActive(false);
    }

    public void TrueExit()
    {
        Application.Quit();
    }


}
