﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public Animator pausePanelAnimator;
    public Animator confirmationPanelAnimator;
    public CharacterInput characterInput;
    public GameObject pausePanel;
    public GameObject confirmationPanel;

    public AudioSource sfxSource;
    public AudioClip clickSound;

    private bool isPaused = false;

    void Awake()
    {
		if (pausePanel != null && confirmationPanel != null) {
			pausePanel.SetActive (true);
			confirmationPanel.SetActive (false);
		}
    }

    void Update()
    {
		if (pausePanel != null && confirmationPanel != null) {
			if (Input.GetKeyDown ("escape")) {
				if (isPaused) {
					UnPause ();
				} else {
					Pause ();
				}
			}
		}
    }

    public void Pause()
    {
        PlayClickSound();
        characterInput.enabled = false;
        pausePanelAnimator.Play("Pause");
        confirmationPanelAnimator.Play("Pause");
        isPaused = !isPaused;
    }

    public void UnPause()
    {
        characterInput.enabled = true;
        pausePanelAnimator.Play("UnPause");
        confirmationPanelAnimator.Play("UnPause");
        isPaused = !isPaused;
    }


    public void EnablePauseMenu()
    {
        pausePanel.SetActive(true);
        confirmationPanelAnimator.Play("UnPause");
        confirmationPanel.SetActive(false);
    }

    public void EnableConfirmQuit()
    {
        pausePanel.SetActive(false);
        confirmationPanel.SetActive(true);
        confirmationPanelAnimator.Play("Pause");
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene(1);
    }
		
    public void SaveGame()
    {
        GameManager.instance.saveData.SaveData();
    }

    public void PlayClickSound()
    {
        sfxSource.PlayOneShot(clickSound);
    }
}
