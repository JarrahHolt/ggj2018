﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OhShit : MonoBehaviour
{
	public static OhShit instance = null;

	void Awake()
	{
		if (!instance)
			instance = this;
		else
			Destroy (gameObject);

		DontDestroyOnLoad (gameObject);
	}

	void Update()
	{

		//FAILSAFES
		if (Input.GetKey (KeyCode.Alpha1)) {
			LoadMainScene ();
		}

		if (Input.GetKey (KeyCode.Alpha2)) {
			LoadSaucyScene ();
		}
	}

	public static void LoadSaucyScene()
	{
		SceneManager.LoadScene ("SaucyScene");
	}

	public static void LoadMainScene()
	{
		SceneManager.LoadScene ("Main");
	}
}
