﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Object = UnityEngine.Object;

public enum PoolType
{
    Default,
    Note
}

public static class PoolManager
{
    private static Dictionary<PoolType, List<GameObject>> pool;

    public static Object Spawn(Object original, Vector3 position = default(Vector3), Quaternion rotation = default(Quaternion), Transform parent = null, PoolType poolType = PoolType.Default)
    {
        // if an equal gameobject is in inactive list, remove and activate
        Object spawnedObject = Object.Instantiate(original, position, rotation, parent);

        return spawnedObject;
    }

    public static bool Despawn(Object target, PoolType poolType = PoolType.Default)
    {
        //deactivate and add to inactive list
        Object.Destroy(target);

        return false;
    }
}
