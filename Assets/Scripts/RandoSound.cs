﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandoSound : MonoBehaviour {

    public AudioSource randomSound;

    public AudioClip[] audioClips;

    void Awake()
    {
        if (randomSound == null)
        {
            randomSound = GetComponent<AudioSource>();
        }
    }

     public void CallSound()
     {
        Debug.Log("Playing Sound");
        randomSound.clip = audioClips[Random.Range(0, audioClips.Length)];
        randomSound.Play ();
        Debug.Log("Played: " + randomSound.name);
    }
}
