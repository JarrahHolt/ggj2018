﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatSpawner : MonoBehaviour
{
    public GameObject prefab;
    public Transform holder;

    [Space]
    public float startDelay = 0f;
    public float repeatRate = 1f;

    private Transform _myTF;
    public Transform myTF
    {
        get
        {
            if (!_myTF)
                _myTF = transform;
            return _myTF;
        }
    }

    void Start()
    {
        InvokeRepeating("SpawnNote", startDelay, repeatRate);

    }

    void SpawnNote()
    {
        Instantiate(prefab, myTF.position, Quaternion.identity, holder);
    }
}
