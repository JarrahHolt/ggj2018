﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class AccuracyPanel : MonoBehaviour
{
	public Transform holder;
	public Text accuracyText;
	public CanvasGroup canvasGroup;

	[Space]
	public float fadeDelay = 1f;
	public float fadeTime = 0.5f;

	[Space]
	public Color missColor = Color.red;
	public Color badColor = Color.grey;
	public Color goodColor = Color.blue;
	public Color perfectColor = Color.yellow;

	private Tweener _fadeTween;

	void Awake()
	{
		TrackManager.onNoteHit += ShowAccuracyText;
	}

	void OnDestroy()
	{
		TrackManager.onNoteHit -= ShowAccuracyText;
	}

	void ShowAccuracyText(HitAccuracy hitAccuracy)
	{
		switch (hitAccuracy) {
		case HitAccuracy.Miss:
			accuracyText.color = missColor;
			break;
		case HitAccuracy.Bad:
			accuracyText.color = badColor;
			break;
		case HitAccuracy.Good:
			accuracyText.color = goodColor;
			break;
		case HitAccuracy.Perfect:
			accuracyText.color = perfectColor;
			break;
		default:
			break;
		}

		ShowText (hitAccuracy.ToString ().ToUpper (), fadeDelay);
	}

	void ShowText(string text, float fadeDelay = 1f)
	{
		StopAllCoroutines ();
		accuracyText.text = text;
		canvasGroup.alpha = 1f;
		StartCoroutine (FadeTextDelayed (fadeDelay));
	}

	IEnumerator FadeTextDelayed(float delay)
	{
		yield return new WaitForSeconds (delay);
		FadeText ();
	}

	void FadeText()
	{
		_fadeTween = DOTween.To (() => canvasGroup.alpha, x => canvasGroup.alpha = x, 0f, fadeTime);
	}
}
