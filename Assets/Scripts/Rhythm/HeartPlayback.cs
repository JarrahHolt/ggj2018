﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HeartPlayback : MonoBehaviour {

    public Transform heartTransform;
    public float finalSize = 1;

    private Tween activeTween;
    private float currentSize = 1;

    private void OnEnable()
    {
        TrackManager.onNoteHit += OnNoteHit;
        Track.onStopTrack += OnSongFinish;
    }

    private void OnDisable()
    {
        TrackManager.onNoteHit -= OnNoteHit;
        Track.onStopTrack -= OnSongFinish;
    }


    private void OnNoteHit(HitAccuracy accuracy)
    {
        if (accuracy != HitAccuracy.Bad && accuracy != HitAccuracy.Miss)
        {
            float temp = finalSize / TrackManager.instance.currentTrack.noteAmount;
            currentSize += temp;
            if (activeTween != null) { activeTween.Kill(); }
            activeTween = DOTween.To(() => heartTransform.localScale, x => heartTransform.localScale = x, Vector3.one * currentSize, 1f).SetEase(Ease.OutElastic);
        }
    }

    private void OnSongFinish()
    {
        if (activeTween != null) { activeTween.Kill(); }
        activeTween = DOTween.To(() => heartTransform.localScale, x => heartTransform.localScale = x, Vector3.zero, 1f).SetEase(Ease.OutExpo);
    }
}
