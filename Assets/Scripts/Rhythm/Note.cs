﻿using UnityEngine;
using DG.Tweening;

public enum HitAccuracy
{
    Miss,
    Bad,
    Good,
    Perfect
}

public class Note : MonoBehaviour
{
	public NoteData noteData;

    public float hitTime;
	public float durationToHit = 1f;

	private Vector3 _spawnerToCatcherDistance;

    private Transform _myTF;
    public Transform myTF
    {
        get
        {
            if (!_myTF)
                _myTF = transform;
            return _myTF;
        }
    }

	void Start()
	{
		_spawnerToCatcherDistance = TrackManager.instance.hitCatchers [noteData.position].position - myTF.position;
		myTF.DOMove (TrackManager.instance.hitCatchers [noteData.position].position, durationToHit).SetEase(Ease.Linear).OnComplete(ContinueMove);
	}

	void ContinueMove()
	{
		myTF.DOMove (myTF.position + _spawnerToCatcherDistance, durationToHit).SetEase(Ease.Linear);
	}

	public void Init(NoteData noteData)
	{
		this.noteData = noteData;
		hitTime = noteData.hitTime;
	}

    void Update()
    {
//        myTF.Translate(Vector3.down * speed * Time.deltaTime);
		if (TrackManager.instance.currentTrack.trackTime > (hitTime + TrackManager.NOTE_DEVIATION))
		{
			TrackManager.instance.OnKeyPressed (noteData.position);
		}
    }
}
