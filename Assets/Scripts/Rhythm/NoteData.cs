﻿using System;

[Serializable]
public class NoteData
{
    public float hitTime;
    public int position = 0;

	private int _id;
	public int id { get { return _id; } set { _id = value; } }
}
