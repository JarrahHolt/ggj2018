﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RhythmInputController : MonoBehaviour
{
    public static RhythmInputController instance = null;

    public static List<KeyCode> hitKeys = new List<KeyCode>()
    {
        KeyCode.A,
        KeyCode.S,
        KeyCode.K,
        KeyCode.L
    };

    public static Action<int> onKeyPressed;

    void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(gameObject);
    }

    void Update()
	{
		for (int i = 0, imax = hitKeys.Count; i < imax; i++) {
			if (Input.GetKeyDown (hitKeys [i])) {
				if (onKeyPressed != null)
					onKeyPressed.Invoke (i);
			}
		}
	}
}
