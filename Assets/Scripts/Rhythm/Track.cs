﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Track : MonoBehaviour
{
	public static Action onStopTrack;

    public TrackData trackData;
    
    public int noteAmount { get { return trackData.noteList.Count; } }

    private float _startTime;
	public float endTime = 1f;
    private float _trackTime = 0f;
    public float trackTime { get { return _trackTime; } }

    private bool _isPlaying = false;

    private Queue<NoteData> noteQueue = new Queue<NoteData>();
	public static List<NoteData> sortedNoteList = new List<NoteData>();

    public void Init()
    {
		sortedNoteList.Clear ();
        sortedNoteList = trackData.noteList.OrderBy(noteData => noteData.hitTime).ToList();
        noteQueue.Clear();
        for (int i = 0, imax = sortedNoteList.Count; i < imax; i++)
        {
			sortedNoteList [i].id = i;
            noteQueue.Enqueue(sortedNoteList[i]);
        }
    }

    public void StartTrack()
    {
        _startTime = Time.time;
		endTime = trackData.songDuration;
        _trackTime = 0f;
        _isPlaying = true;
    }

    public void StopTrack()
    {
        _isPlaying = false;

		if (onStopTrack != null)
			onStopTrack.Invoke ();
		
		Invoke ("LoadGoodScene", 5f);
    }

	public void LoadGoodScene()
	{
		if (TrackManager.totalScorePercentage > 0.8f)
			OhShit.LoadSaucyScene ();
		else
			OhShit.LoadMainScene ();
	}

    void Update()
    {
        if (_isPlaying)
        {
            _trackTime += Time.deltaTime;
            TrySpawnNote();

			if (_trackTime >= endTime) {
				StopTrack ();
			}
        }
    }

    void TrySpawnNote()
    {
        if (noteQueue.Count > 0 && noteQueue.Peek().hitTime - trackData.noteLifetime <= _trackTime)
        {
            TrackManager.instance.SpawnNote(noteQueue.Dequeue());
            TrySpawnNote();
        }
    }
}
