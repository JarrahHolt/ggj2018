﻿using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class TrackData
{
    public float noteLifetime = 1f;
	public float songDuration = 5f;
	public AudioClip song;
    public List<NoteData> noteList;
}
