﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TrackManager : MonoBehaviour
{
    public static TrackManager instance = null;

    public const float NOTE_VALUE_MISS = 0f;
    public const float NOTE_VALUE_BAD = 0.4f;
    public const float NOTE_VALUE_GOOD = 0.75f;
    public const float NOTE_VALUE_PERFECT = 1f;

	public const float NOTE_DEVIATION = 0.5f;
	public const float NOTE_LENIENCY = 0.2f;

	public const float NOTE_VALUE_MULTIPLIER = 2000f;

	public static Action<HitAccuracy> onNoteHit;

	public AudioSource audioSource;
    public Note notePrefab;

    [Space]
	public static TrackSO trackToLoad;
    public Track currentTrack;
	public static float totalScore;
	public static float totalScorePercentage;

    [Header("Track Positioning")]
    public Transform noteHolder;
    public List<Transform> hitSpawners = new List<Transform>();
    public List<Transform> hitCatchers = new List<Transform>();

    private List<Queue<Note>> _notesByPosition = new List<Queue<Note>>();

    void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(gameObject);

        RhythmInputController.onKeyPressed += OnKeyPressed;
    }

	void OnDestroy()
	{
		RhythmInputController.onKeyPressed -= OnKeyPressed;
	}

	public void LoadTrack(TrackSO track)
	{
		currentTrack.trackData = track.trackData;
		audioSource.clip = track.trackData.song;
	}

    void Start()
    {
		if (trackToLoad != null)
			LoadTrack (trackToLoad);

		Invoke ("StartTrack", 2f);
    }

	void StartTrack()
	{
		_notesByPosition.Clear ();
		for (int i = 0, imax = hitCatchers.Count; i < imax; i++)
		{
			_notesByPosition.Add(new Queue<Note>());
		}

		totalScore = 0f;
		totalScorePercentage = 0f;
		audioSource.Play ();
		currentTrack.Init ();
		currentTrack.StartTrack ();
	}

    public Note SpawnNote(NoteData noteData)
    {
        Note spawnedNote = (Note)PoolManager.Spawn(notePrefab, hitSpawners[noteData.position].position, parent: noteHolder, poolType: PoolType.Note);
		spawnedNote.Init (noteData);
        _notesByPosition[noteData.position].Enqueue(spawnedNote);
        return spawnedNote;
    }

    public void OnKeyPressed(int hitIndex)
    {
		if (_notesByPosition [hitIndex].Count == 0)
			return;

		float noteHitTime = ((Note)_notesByPosition[hitIndex].Peek()).hitTime;
		if (_notesByPosition [hitIndex].Count > 0 && (currentTrack.trackTime >= (noteHitTime - NOTE_DEVIATION) && currentTrack.trackTime <= (noteHitTime + NOTE_DEVIATION))) {
			float distance = Mathf.Abs (currentTrack.trackTime - noteHitTime);
			Note hitNote = (Note)_notesByPosition [hitIndex].Dequeue ();

			float normalizedDistance = Mathf.InverseLerp (NOTE_DEVIATION, 0f, distance);
			HitAccuracy hitAccuracy;
			if (normalizedDistance >= NOTE_VALUE_PERFECT - NOTE_LENIENCY)
				hitAccuracy = HitAccuracy.Perfect;
			else if (normalizedDistance >= NOTE_VALUE_GOOD - NOTE_LENIENCY)
				hitAccuracy = HitAccuracy.Good;
			else if (normalizedDistance >= NOTE_VALUE_BAD - NOTE_LENIENCY)
				hitAccuracy = HitAccuracy.Bad;
			else
				hitAccuracy = HitAccuracy.Miss;

			if (onNoteHit != null)
				onNoteHit.Invoke (hitAccuracy);

			UpdateScore (hitAccuracy, hitNote.noteData.id);

			PoolManager.Despawn (hitNote.gameObject, PoolType.Note);
		}
		else if (currentTrack.trackTime > (noteHitTime + NOTE_DEVIATION))
		{
			Note hitNote = (Note)_notesByPosition [hitIndex].Dequeue ();
			if (onNoteHit != null)
				onNoteHit.Invoke (HitAccuracy.Miss);

			UpdateScore (HitAccuracy.Miss, hitNote.noteData.id);
			PoolManager.Despawn (hitNote.gameObject, PoolType.Note);
		}
    }

	public static void RaiseOnNoteHit(HitAccuracy hitAccuracy)
	{
		if (onNoteHit != null)
			onNoteHit.Invoke (hitAccuracy);
	}

	public void UpdateScore(HitAccuracy hitAccuracy, int noteId)
	{
		totalScore += GetHitPointValue (hitAccuracy);
		totalScorePercentage += GetHitRealValue (hitAccuracy);

		switch (hitAccuracy) {
		case HitAccuracy.Miss:
		case HitAccuracy.Bad:
			int charsPerNote = Mathf.FloorToInt((float)currentTrack.noteAmount * 0.01f * (float)ConversationManager.GetCurrentDialogueLength());
			ConversationManager.ReplaceDialogueAt(charsPerNote * noteId, charsPerNote);
			break;
		default:
			break;
		}
	}

    public static float GetHitRealValue(HitAccuracy hitAccuracy)
    {
        float hitValue = 0f;

        switch (hitAccuracy)
        {
            case HitAccuracy.Miss:
                hitValue = instance.currentTrack.noteAmount * 0.01f * NOTE_VALUE_MISS;
                break;
            case HitAccuracy.Bad:
                hitValue = instance.currentTrack.noteAmount * 0.01f * NOTE_VALUE_BAD;
                break;
            case HitAccuracy.Good:
                hitValue = instance.currentTrack.noteAmount * 0.01f * NOTE_VALUE_GOOD;
                break;
            case HitAccuracy.Perfect:
                hitValue = instance.currentTrack.noteAmount * 0.01f * NOTE_VALUE_PERFECT;
                break;
            default:
                break;
        }

        return hitValue;
    }

	public static float GetHitPointValue(HitAccuracy hitAccuracy)
	{
		return GetHitRealValue (hitAccuracy) * NOTE_VALUE_MULTIPLIER;
	}
}
