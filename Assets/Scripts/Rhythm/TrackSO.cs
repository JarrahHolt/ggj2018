﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;

[CreateAssetMenu(fileName = "Track", menuName = "Track")]
public class TrackSO : ScriptableObject
{
	public TrackData trackData;
}
