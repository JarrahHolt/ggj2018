﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class AnimationSequence
{
    public Animation _animation;
    public AnimationClip _animationClip;
    public float _delay;
}

public class AnimationSequencer : MonoBehaviour {

    [SerializeField] private List<AnimationSequence> _animationList;
    public UnityEvent _onSequenceCompleteAction;

    //
    private int _count;

    //
    public void PlaySequence()
    {
        PlayNextAnimation();
    }

    public void PlayNextAnimation()
    {
        AnimationSequence initialSequence = _animationList[_count];
        StartCoroutine(AnimationSequenceRoutine(initialSequence._animation,initialSequence._animationClip, initialSequence._delay));
    }

    public IEnumerator AnimationSequenceRoutine(Animation animation, AnimationClip animationClip, float delay)
    {
        yield return null;
        yield return new WaitForSeconds(delay);
        //
        animation.clip = animationClip;
        animation.Play(PlayMode.StopAll);
        while (animation.isPlaying)
        {
            yield return null;
        }
        _count++;
        if (_count <= _animationList.Count)
        {
            PlayNextAnimation();
        } else
        {
            if (_onSequenceCompleteAction != null)
            {
                _onSequenceCompleteAction.Invoke();
            }
        }
    }

}
