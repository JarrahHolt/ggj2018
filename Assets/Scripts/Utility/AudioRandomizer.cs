﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacterName { Square, Triangle, Bean, Star, Pentagon, VV };

[System.Serializable]
public struct CharacterAudio
{
    public CharacterName characterName;
    public AudioClip[] audioClips;
}

[RequireComponent(typeof(AudioSource))]
public class AudioRandomizer : MonoBehaviour {

    public CharacterAudio[] characterAudio;

    private Dictionary<CharacterName, AudioClip[]> audioDictionary;
    private AudioSource audioSource;
    private bool isAlreadyPlaying = false;

    private void OnEnable()
    {
        Init();
        PlayAudioDialog(CharacterName.Triangle, 4);
    }

    public void Init()
    {
        if (audioDictionary == null)
        {
            audioDictionary = new Dictionary<CharacterName, AudioClip[]>();
            for (int i = 0; i < characterAudio.Length; i++)
            {
                if (!audioDictionary.ContainsKey(characterAudio[i].characterName))
                {
                    audioDictionary.Add(characterAudio[i].characterName, characterAudio[i].audioClips);
                }
                else
                {
                    Debug.LogError("[Audio Randomizer] : Attempting to add a key to dictionary which already exists. This is not allowed.");
                }
            }
        }
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayAudioDialog(CharacterName name, int soundLength)
    {
        if (audioSource != null && !isAlreadyPlaying)
        {
            if (audioDictionary.ContainsKey(name))
            {
                StartCoroutine(PlayAudioDialogRoutine(name, soundLength));
            } else
            {
                Debug.Log("No audio for preson");
            }
        } else
        {
            Debug.Log("Either audio source is null or character sounds are already playing");
        }
    }

    private IEnumerator PlayAudioDialogRoutine(CharacterName name, int soundLength)
    {
        isAlreadyPlaying = true;
        //
        AudioClip[] musicArray = audioDictionary[name];
        for (int i = 0; i < soundLength; i++)
        {
            audioSource.PlayOneShot(musicArray[Random.Range(0, musicArray.Length)]);
            while (audioSource.isPlaying)
            {
                yield return null;
            }
        }
        //
        isAlreadyPlaying = false;
    }

}
