﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class FadeLoadScene : MonoBehaviour
{
	public float loadDelay = 1f;
	public CanvasGroup canvasGroup;

	private bool _isLoading = false;

	public void FadeLoad(string sceneName)
	{
		if (!_isLoading)
		{
			_isLoading = true;
			DOTween.To (() => canvasGroup.alpha, x => canvasGroup.alpha = x, 1f, loadDelay).OnComplete (() => SceneManager.LoadScene (sceneName));
		}
	}
}
