﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MonobehaviourEvent : MonoBehaviour {

    public UnityEvent OnEnableEvent;
    public UnityEvent OnDisableEvent;
    public UnityEvent OnAwakeEvent;
    public UnityEvent OnStartEvent;
    public UnityEvent OnUpdateEvent;
    public UnityEvent OnDestroyEvent;

    private void OnEnable()
    {
        if (OnEnableEvent != null)
        {
            OnEnableEvent.Invoke();
        }
    }

    private void OnDisable()
    {
        if (OnDisableEvent != null)
        {
            OnDisableEvent.Invoke();
        }
    }

    private void Awake()
    {
        if (OnAwakeEvent != null)
        {
            OnAwakeEvent.Invoke();
        }
    }

    private void Start()
    {
        if (OnStartEvent != null)
        {
            OnStartEvent.Invoke();
        }
    }

    private void Update()
    {
        if (OnUpdateEvent != null)
        {
            OnUpdateEvent.Invoke();
        }
    }

    private void OnDestroy()
    {
        if (OnDestroyEvent != null)
        {
            OnDestroyEvent.Invoke();
        }
    }


}
