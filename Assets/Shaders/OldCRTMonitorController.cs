﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class OldCRTMonitorController : MonoBehaviour {

    public const string CRT_MONITOR_SHADER_NAME = "ImageEffect/OldCRTMonitor";

    [SerializeField] private Shader _curShader;
    //
    [FormerlySerializedAs("DistortionAmount")]
    [SerializeField] private float distortionAmount = 0.1f;
    [SerializeField] private Vector2 scanSize =  new Vector2(1024,1024);
    [SerializeField] private Color scanLineColour = Color.grey;
    //
    [SerializeField] [Range(0f,1f)] private float cutoff = 0;
    [SerializeField] private Texture2D transitionImage;

    private Shader curShader
    {
        get
        {
            if (_curShader == null)
            {
                _curShader = Shader.Find(CRT_MONITOR_SHADER_NAME);
            }
            return _curShader;
        }
    }
    private Material curMaterial;
    Material material
    {
        get
        {
            if (curMaterial == null)
            {
                curMaterial = new Material(curShader);
                curMaterial.hideFlags = HideFlags.HideAndDontSave;
            }
            return curMaterial;
        }
    }

    void Start()
    {
        if (!SystemInfo.supportsImageEffects)
        {
            this.enabled = false;
        }
    }

    private void UpdateMaterialValues()
    {
        material.SetFloat("_Distortion", distortionAmount);
        material.SetVector("_ScanSize", scanSize);
        material.SetColor("_ScanLineCol", scanLineColour);
        material.SetFloat("_Cutoff", cutoff);
        material.SetTexture("_TransitionTex", transitionImage);
    }

    void OnDisable()
    {
        if (curMaterial)
        {
            if (Application.isPlaying){ Destroy(curMaterial); }
            else { DestroyImmediate(curMaterial); }
        }

    }

    private void OnValidate()
    {
        UpdateMaterialValues();
    }

    void OnRenderImage(RenderTexture sourceTexture, RenderTexture destTexture)
    {
        UpdateMaterialValues();
        Graphics.Blit(sourceTexture, destTexture, material);
    }

}
