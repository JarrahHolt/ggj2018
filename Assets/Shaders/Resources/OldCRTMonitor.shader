﻿Shader "ImageEffect/OldCRTMonitor"
{
    Properties
    {
        _MainTex ("Base (RGB)", 2D) = "white" {}
		_TransitionTex ("Transition", 2D) = "white" {}
		_Cutoff ("Cutoff", Range(0,1)) = 0
    }
   
    SubShader
    {
		Cull Off zWrite Off ZTest Always

        Pass
        {
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			#include "UnityCG.cginc"
			#pragma target 3.0

			sampler2D _MainTex;
			float4 _MainTex_TexelSize;
			float2 _ScanSize;
			float _Distortion = 0.1f;
			float4 _ScanLineCol;
			//
			sampler2D _TransitionTex;
			float _Cutoff;
			fixed4 _Color = fixed4(0,0,0,1);

			float2 MonitorDistortion(float2 texCoord)
			{
				float2 cc = texCoord - 0.5f;
				float dist = dot(cc, cc) * _Distortion;
				return (texCoord + cc * (0.5f + dist) * dist);
			}
     
			float4 ScanlineWeights(float distance, float4 color)
			{
				float4 width = 2.0f + 2.0f * pow(color, float4(4.0f, 4.0f, 4.0f, 4.0f));
				float4 weights = float4(distance / 0.5f, distance / 0.5f, distance / 0.5f, distance / 0.5f);
				return 1.4f * exp(-pow(weights * rsqrt(0.5f * width), width)) / (0.3f + 0.2f * width);
			}

			float4 frag(v2f_img i) : COLOR
			{
				float2 textureSize = _ScanSize;
				float factor = i.uv.x * textureSize.x;
				
				float2 xy = MonitorDistortion(i.uv);
         
				float2 ratio = xy * textureSize - float2(0.5f, 0.5f);
				float2 uvratio = frac(ratio);
         
				xy.y = floor(ratio.y) / textureSize;
				float4 col = tex2D(_MainTex, xy);
				float4 col2 = tex2D(_MainTex, xy + float2(0.0f, (1.0f / textureSize).y));
         
				float4 weights = ScanlineWeights(uvratio.y, col);
				float4 weights2 = ScanlineWeights(1.0f - uvratio.y, col2);

				float3 res = (col * weights + col2 * weights2).rgb;
         
				float3 dotMaskWeights = lerp(_ScanLineCol.rgb, float3(1,1,1), floor(fmod(factor, 2)));
				res *= dotMaskWeights;
         
				//Transition Stuff
				float4 cutoff = tex2D(_TransitionTex,i.uv);

				res = lerp(res, _Color, _Cutoff + (1- cutoff.b));

				return float4(res, 1.0f);
			}
			ENDCG
        }
    }
}
